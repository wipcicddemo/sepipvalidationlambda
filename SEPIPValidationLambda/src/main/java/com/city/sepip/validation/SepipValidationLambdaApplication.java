package com.city.sepip.validation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SepipValidationLambdaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SepipValidationLambdaApplication.class, args);
	}

}
